# magicscript__compare_table

magicscript__compare_table is a stored procedure to manually compare two tables.

## How to use:

```

declare @excluded_columns_xml xml = '
    <Columns>
        <Name>CreatedTime</Name>
        <Name>DeletedTime</Name>
    </Columns>'


exec [dbo].[magicscript__compare_tables] 
    @left_table = N'db_name..tb_name_1',
    @right_table = N'db_name..tb_name_2',
    @not_comparable_columns = @excluded_columns_xml
    -- if next arg isn't set or null, procedure doesn't store results to table
    --@results_to_table = 'tb_name'  
    -- by default it uses primiry keys
    --@key_fields = 
    --  N'<Columns>
    --      <Name>Name</Name>
    --  </Columns>'
```
	
	
## Another case:

```
exec [dbo].[magicscript__compare_tables] 
    @left_table = N'db_name..tb_name_1',
    @right_table = N'db_name..tb_name_2',
    @key_fields = 
    N'<Columns>
        <Name>Name</Name>
    </Columns>'
```



